-include env.mk

GO ?= go
CHROOT = stretch-amd64-sbuild

.PHONY: all
all: assets build


.PHONY: clean
clean:
	rm -f irclog
	rm -f modules/assets/assets_vfsdata.go
	rm -rf web/dist


.PHONY: build
build:
	$(GO) generate
	$(GO) build -ldflags="-s -w"


.PHONY: assets
assets:
	(cd web && npm ci && npm run build)


.PHONY: dev
dev:
	$(GO) run -tags=dev main.go


.PHONY: dch
dch:
	dch --distribution "unstable" "New upstream release"


.PHONY: debuild
debuild:
	sbuild -c $(CHROOT)
