package main

import (
	"log"
	"time"
	"net/http"

	"github.com/crgimenes/goconfig"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"nori.one/irclog/modules/api"
	"nori.one/irclog/modules/assets"
)

//go:generate go run -tags=dev bin/assets_generate.go

type config struct {
	LogRoot string	`cfgRequired:"true"`
	Listen	string	`cfgDefault:":5000"`
	Debug	bool	`cfg:"Debug" cfgDefault:"false"`
}


func main() {
	cfg := config{}
	err := goconfig.Parse(&cfg)

	if err != nil {
		panic(err)
	}

	router := chi.NewRouter()
	router.Use(middleware.Recoverer)

	if cfg.Debug {
		router.Use(middleware.Logger)
	}

	router.Use(api.LogRoot(cfg.LogRoot))

	router.Handle("/*", http.FileServer(assets.Assets))

	router.Route("/api", func(r chi.Router) {
		r.Get("/", api.ServerList)
		r.Get("/channels/{network}/{channel}", api.ChannelLogs)
		r.Get("/channels/{network}/{channel}/{date}", api.ChannelLogEntry)
	})

	s := &http.Server{
		Addr:           cfg.Listen,
		Handler:        router,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	done := make(chan bool)
	go s.ListenAndServe()
	log.Printf("IRCLog server started on %s", cfg.Listen)
	<-done
}
