package api

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"

	"nori.one/irclog/modules/models"
)


type NetworkReturn struct {
	Name string					`json:"name"`
	Channels []ChannelReturn	`json:"channels"`
}

type ChannelReturn struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type LogEntryReturn struct {
	Date string					`json:"date"`
	Prev string					`json:"prev,omitempty"`
	Next string					`json:"next,omitempty"`
	Items []models.LogItem		`json:"items"`
}


func LogRoot(path string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), "logroot", path)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}


func ServerList(w http.ResponseWriter, r *http.Request) {
	netList, err := models.NewNetworkList(r.Context().Value("logroot").(string))
	if err != nil {
		http.NotFound(w, r)
		return
	}

	networks, err := netList.GetNetworks()
	if err != nil {
		http.NotFound(w, r)
		return
	}

	res := []NetworkReturn{}

	for _, n := range networks {
		chans, _ := netList.GetNetworkChannels(n.Name)
		ret := NetworkReturn{Name: n.Name}
		for _, chn := range chans {
			t := "channel"
			if !strings.HasPrefix(chn.Name, "#") {
				t = "query"
			}

			ret.Channels = append(ret.Channels, ChannelReturn{
				Name: chn.Name,
				Type: t,
			})
		}
		res = append(res, ret)
	}

	render.JSON(w, r, res)
}


func ChannelLogs(w http.ResponseWriter, r *http.Request) {
	netList, err := models.NewNetworkList(r.Context().Value("logroot").(string))
	if err != nil {
		http.NotFound(w, r)
		return
	}

	logs, err := netList.GetChannelLogs(
		chi.URLParam(r, "network"), chi.URLParam(r, "channel"))

	if err != nil {
		http.NotFound(w, r)
		return
	}

	render.JSON(w, r, logs)
}


func ChannelLogEntry(w http.ResponseWriter, r *http.Request) {
	netList, err := models.NewNetworkList(r.Context().Value("logroot").(string))
	if err != nil {
		http.NotFound(w, r)
		return
	}

	dt, err := time.Parse("20060102", chi.URLParam(r, "date"))
	logs, err := netList.GetChannelLogs(chi.URLParam(r, "network"), chi.URLParam(r, "channel"))

	if err != nil {
		http.NotFound(w, r)
		return
	}

	if err != nil {
		http.NotFound(w, r)
		return
	}
	var prev, next string
	var d_ []byte
	for i, t := range logs {
		if t.Date.Unix() == dt.Unix() {
			if i > 0 {
				d_, _ = logs[i - 1].Date.MarshalText()
				prev = fmt.Sprintf("%s", d_)
			}
			if i + 1 < len(logs) {
				d_, _ = logs[i + 1].Date.MarshalText()
				next = fmt.Sprintf("%s", d_)
			}
			break
		}
	}


	items, err := netList.GetLog(chi.URLParam(r, "network"), chi.URLParam(r, "channel"), chi.URLParam(r, "date"))
	if err != nil {
		http.NotFound(w, r)
		return
	}

	d_, _ = dt.MarshalText()

	res := LogEntryReturn{
		Date: fmt.Sprintf("%s", d_),
		Prev: prev,
		Next: next,
		Items: items,
	}

	render.JSON(w, r, res)
}
