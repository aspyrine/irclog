package models

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"
)

var regLine = regexp.MustCompile(`^(?:\[(\d{2}):(\d{2}):(\d{2})\])?(?: <(.+?)>)?(.*)`)
var regSelfMsg = regexp.MustCompile(`^\* (.+?) (.*)$`)

type networkList struct {
	Root string
}

type network struct {
	Name string `json:"name"`
}

type channel struct {
	Name string `json:"name"`
}

type logFile struct {
	Date time.Time `json:"date"`
}

type LogItem struct {
	Time time.Time	`json:"time"`
	Self bool		`json:"self"`
	Nick string		`json:"nick"`
	Msg string		`json:"msg"`
}


func NewNetworkList (path string) (*networkList, error) {
	res := new(networkList)

	root, err := getDir(path)

	if err != nil {
		return res, err
	}

	res.Root = root
	return res, nil
}


func (instance networkList) GetNetworks() ([]network, error) {
	res := []network{}

	entries, err := listDir(instance.Root)
	if err != nil {
		return nil, err
	}

	for _, x := range entries {
		res = append(res, network{Name: x.Name()})
	}

	return res, nil
}


func (instance networkList) GetNetworkChannels(netName string) ([]channel, error) {
	res := []channel{}

	entries, err := listDir(filepath.Join(instance.Root, netName))
	if err != nil {
		return nil, err
	}

	for _, x := range entries {
		res = append(res, channel{Name: x.Name()})
	}

	sort.SliceStable(res, func(a, b int) bool {
		return res[a].Name < res[b].Name
	})

	return res, nil
}


func (instance networkList) GetChannelLogs(netName, chanName string) ([]logFile, error) {
	res := []logFile{}

	entries, err := listDir(filepath.Join(instance.Root, netName, chanName))
	if err != nil {
		return nil, err
	}

	p := "2006-01-02.log"
	for _, x := range entries {
		d, _ := time.Parse(p, x.Name())
		res = append(res, logFile{Date: d})
	}

	sort.SliceStable(res, func(a, b int) bool {
		return res[a].Date.Unix() < res[b].Date.Unix()
	})

	return res, nil
}


func (instance networkList) GetLog(netName, chanName, date string) ([]LogItem, error) {
	dt, _ := time.Parse("20060102", date)

	path := filepath.Join(
		instance.Root, netName, chanName,
		fmt.Sprintf(dt.Format("2006-01-02.log")))

	file, err := os.Open(path)
	if err != nil {
		return []LogItem{}, err
	}
	defer file.Close()

	res := []LogItem{}
	scanner := bufio.NewScanner(file)
    for scanner.Scan() {
    	logLine := parseLogLine(dt, scanner.Text())
    	if logLine != nil {
        	res = append(res, *logLine)
        }
    }

	return res, nil
}


func parseLogLine (dt time.Time, line string) *LogItem {
	if !regLine.MatchString(line) {
		return nil
	}

	m := regLine.FindStringSubmatch(line)
	duration, _ := time.ParseDuration(fmt.Sprintf("%sh%sm%ss", m[1], m[2], m[3]))
	time := dt.Add(duration)
	nick := m[4]
	msg := strings.Trim(m[5], " ")
	self := false

	if nick == "" {
		if regSelfMsg.MatchString(msg) {
			m = regSelfMsg.FindStringSubmatch(msg)
			self = true
			nick = m[1]
			msg = m[2]
		}
	}

	return &LogItem{
		Time: time,
		Nick: nick,
		Self: self,
		Msg: msg,
	}
}


func getDir(path string) (string, error) {
	root, err := filepath.Abs(path)

	if err != nil {
		return "", err
	}

	st, err := os.Stat(root)
	if err != nil {
		return "", err
	}

	if !st.IsDir() {
		return "", errors.New(fmt.Sprintf("%s is not a directory", root))
	}

	return root, nil
}


func listDir(path string) ([]os.FileInfo, error) {
	st, err := os.Stat(path)

	if err != nil {
		return nil, err
	}

	if !st.IsDir() {
		return nil, errors.New(fmt.Sprintf("%s is not a directory", path))
	}

	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	return files, nil
}
