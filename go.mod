module nori.one/irclog

go 1.13

require (
	github.com/crgimenes/goconfig v1.2.1
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
)
