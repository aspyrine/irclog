import Vue from 'vue';
import Router from 'vue-router';

import Main from './views/Main.vue';
import Channel from './views/Channel.vue';
import Conversation from './views/Conversation.vue';

Vue.use(Router);

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/:network/:id',
      name: 'channel',
      component: Channel
    },
    {
      path: '/:network/:id/:day(\\d{8})',
      name: 'conversation',
      component: Conversation
    }
  ]
});
