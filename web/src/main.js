import Vue from 'vue'
import App from './App.vue'
import router from './router'
import logReader from './lib/log-reader';

import './assets/style.styl';

Vue.config.productionTip = false

Vue.prototype.$logReader = new logReader('/api');

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
