'use strict';
import axios from 'axios';
import moment from 'moment';


class logReader {
  constructor (rootUri) {
    this.rootUri = rootUri;
    this.request = axios.create({
      baseURL: this.rootUri
    });
  }

  async getNetworks () {
    let r = await this.request.get('');
    return r.data.map(n => {
      n.key = n.name;
      n.channels = n.channels.map(c => {
        c.key = `${n.key}-${c.name}`
        return c;
      });
      return n;
    });
  }

  async getChannel (network, name) {
    let p = `/channels/${network}/${encodeURIComponent(name)}`;
    let r = await this.request.get(p);

    return r.data.reduce((acc, cur) => {
      let d = moment(cur.date);
      let y = d.year();
      let m = d.month() + 1;

      acc[y] = acc[y] || {};
      acc[y][m] = acc[y][m] || [];
      acc[y][m].push(d);
      return acc;
    }, {});
  }

  async getConversation (network, channel, day) {
    let p = `/channels/${network}/${encodeURIComponent(channel)}/${day.format('YYYYMMDD')}`;
    let r = await this.request.get(p);

    let res = r.data;
    res.date = moment(res.date);
    if (res.prev) {
      res.prev = moment(res.prev);
    }
    if (res.next) {
      res.next = moment(res.next);
    }
    res.items = res.items.map(x => {
      x.time = moment(x.time);
      return x;
    });

    return res;
  }
}

export default logReader;
