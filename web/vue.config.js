module.exports = {
  lintOnSave: false,
  devServer: {
    host: '127.0.0.1',
    port: 8000,
    proxy: {
      '^/api': {
        target: 'http://localhost:5000',
        changeOrigin: true
      }
    }
  }
}
